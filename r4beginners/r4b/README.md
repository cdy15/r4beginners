> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Cy Yates

### r4beginners Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Follow Tutorial in r4beginners.pdf
3. Include 5 graphs to display data
4. Bitbucket repo links:
	* My LIS4369 bitbucket homepage

#### README.md file should include the following items:

* Screenshot of *Facebook Stock Analysis* application running
* Screenshot of *MPG compared with engine displacement* 
* Screenshot of *MPG data graphed with ggplot2* 
* Screenshot of *Sample pressure graphed with ggplot2* 
* Screenshot of *Cylinder Count graphed with ggplot2*

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Facebook Stock Graph*:

![Screenshot of Facebook Stock:](img/fb_stock.png)

*Screenshot of MPG compared with engine displacement*:

![Screenshot of MPG compared with engine displacement:](img/mpg_displacement.png)

*Screenshot of MPG data graphed with ggplot2*:

![Screenshot of MPG data graphed with ggplot2:](img/mpg_ggplot2.png)

*Screenshot of Sample pressure graphed with ggplot2*:

![Screenshot of Sample pressure graphed with ggplot2:](img/pressure_line.png)

*Screenshot of Cylinder Count graphed with ggplot2*:

![Screenshot of SCylinder Count graphed with ggplot2:](img/cyl_count.png)




#### Bitbucket Links:

*Cy's LIS4369 Homepage:*
<https://bitbucket.org/cdy15/lis4369>

