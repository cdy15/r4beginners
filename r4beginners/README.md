> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369

## Cy Yates

### R4Beginners Assignment:

*Course Work Links:*

1. [r4beginners README.md](r4b/README.md "My r4beginners README.md file")
	- Create a document describing all listed commands
	- Execute some commands that I learned
	- Create at least 5 different graphs